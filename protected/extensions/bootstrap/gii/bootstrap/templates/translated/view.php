<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('site','$label')=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>Yii::t('site','Update <?php echo $this->modelClass; ?>'),'url'=>array('update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>), 'icon'=>'pencil'),
	array('label'=>Yii::t('site','Delete <?php echo $this->modelClass; ?>'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Are you sure you want to delete this item?'), 'icon'=>'remove'),
	array('label'=>Yii::t('site','Create New <?php echo $this->modelClass; ?>'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('site','List <?php echo $label; ?>'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('site','Manage <?php echo $label; ?>'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo "<?php echo CHtml::encode(Yii::t('site','View {$this->class2name($this->modelClass)} #{primaryKey}',array('{primaryKey}'=>\$model->{$this->tableSchema->primaryKey}))); ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>
