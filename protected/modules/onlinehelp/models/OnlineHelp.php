<?php

/**
 * This is the model class for table "{{onlinehelp}}".
 *
 * The followings are the available columns in table '{{onlinehelp}}':
 * @property integer $id
 * @property string $title
 * @property string $requestId
 * @property boolean $isPageHelp
 * @property integer $sequenceNbr
 * @property string $elementId
 * @property string $guiderPosition
 * @property string $content
 * @property integer $tsCreated
 * @property integer $userIdCreated
 * @property integer $tsUpdated
 * @property integer $userIdUpdated
 */
class OnlineHelp extends Model //CActiveRecord
{
	// {{{ *** Members ***
    /**
     * @var string
     */
    public $nameFields = "title;requestId";
    /**
     * @var string
     */
    public $nameFormat = "{1} ({2})";

    public $saveAsNew=false;

    // }}} End Members
	// {{{ *** Methods ***
	// {{{ model
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OnlineHelp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	} // }}} 	    
	// {{{ tableName
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{onlinehelp}}';
	} // }}} 
    // {{{ behaviors
    public function behaviors()
    {
        return array();
        return array(
            'TimestampBehavior' => array(
                'class' => 'application.components.behaviors.TimestampBehavior',
                'tsCreateAttribute' => 'tsCreated',
                'userCreateAttribute' => 'userIdCreated',
                'tsUpdateAttribute' => 'tsUpdated',
                'userUpdateAttribute' => 'userIdUpdated',
            )
        );
    } // }}} 
    // {{{ rules
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content', 'required'),
            array('title, requestId, elementId', 'length', 'max'=>255),
            array('guiderPosition', 'length', 'max'=>15),
            array('isPageHelp', 'boolean'),
            array('sequenceNbr', 'numerical', 'integerOnly'=>true, 'min'=>1),
            array('saveAsNew', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, requestId, isPageHelp, sequenceNbr, elementId, guiderPosition, content, tsCreated, userIdCreated, tsUpdated, userIdUpdated', 'safe', 'on'=>'search'),
		);
	} // }}} 
	// {{{ relations
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                return array(
                    'userCreated'=>array(self::BELONGS_TO,'User','userIdCreated'),
                    'userUpdated'=>array(self::BELONGS_TO,'User','userIdUpdated'),
		);
	} // }}} 
	// {{{ attributeLabels
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('onlinehelp','ID'),
			'title' => Yii::t('onlinehelp','Title'),
            'requestId' => Yii::t('onlinehelp','Request ID'),
            'isPageHelp' => Yii::t('onlinehelp','Help for complete page'),
            'sequenceNbr' => Yii::t('onlinehelp','Sequence No.'),
            'elementId' => Yii::t('onlinehelp','Element ID'),
            'guiderPosition' => Yii::t('onlinehelp','Guider Position'),
			'content' => Yii::t('onlinehelp','Content'),
			'tsCreated' => Yii::t('onlinehelp','Created On'),
			'userIdCreated' => Yii::t('onlinehelp','Created By'),
			'tsUpdated' => Yii::t('onlinehelp','Updated On'),
            'userIdUpdated' => Yii::t('onlinehelp','Updated By'),
            'saveAsNew' => Yii::t('onlinehelp','Save as New'),
		);
	} // }}} 
	// {{{ search
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('requestId',$this->requestId,true);
        $criteria->compare('isPageHelp',$this->isPageHelp,true);
        $criteria->compare('sequenceNbr',$this->sequenceNbr,true);
        $criteria->compare('elementId',$this->elementId,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('tsCreated',$this->tsCreated);
        $criteria->compare('userIdCreated',$this->userIdCreated);
        $criteria->compare('tsUpdated',$this->tsUpdated);
        $criteria->compare('userIdUpdated',$this->userIdUpdated);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'requestId, sequenceNbr',
            ),
	    ));
    } // }}} 
    // {{{ getRecordName
    public function getRecordName()
    {
        $name = $this->requestId.'/#'.$this->sequenceNbr.'/'.$this->title;
        if(trim($this->elementId)!=='')
            $name .= '/'.$this->elementId;
        return $name;
    } // }}} 
    // {{{ getGuiderPositionOptions
    /**
     * Returns an array of Guider position names, indexed by position codes
     *
     * @return mixed
     */
    public function getGuiderPositionOptions()
    {
        return array(
            11=>"Top Left (11 o'clock)",
            12=>"Top (12 o'clock)",
            1=>"Top Right (1 o'clock)",
            2=>"Right Top (2 o'clock)",
            3=>"Right (3 o'clock)",
            4=>"Right Bottom (4 o'clock)",
            5=>"Bottom Right (5 o'clock)",
            6=>"Bottom (6 o'clock)",
            7=>"Bottom Left (7 o'clock)",
            8=>"Left Bottom (8 o'clock)",
            9=>"Left (9 o'clock)",
            10=>"Left Top (10 o'clock)"
        );
    } // }}} 
    // }}} End Methods
}
