-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;

DROP TABLE IF EXISTS `tbl_onlinehelp`;
CREATE TABLE `tbl_onlinehelp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requestId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isPageHelp` tinyint(1) NOT NULL COMMENT '1 to show this help as page help, 0 for element help',
  `sequenceNbr` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Sequence of help content for elements',
  `elementId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guiderPosition` varchar(15) COLLATE 'utf8_unicode_ci' NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tsCreated` int(11) NOT NULL,
  `userIdCreated` int(11) NOT NULL,
  `tsUpdated` int(11) NOT NULL,
  `userIdUpdated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `requestId` (`requestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Online Help Messages';


DROP TABLE IF EXISTS `tbl_useronlinehelptohide`;
CREATE TABLE `tbl_useronlinehelptohide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `requestId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId_requestId` (`userId`,`requestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Online Help Request ID''s to hide per user';


-- 2013-01-10 13:08:45
