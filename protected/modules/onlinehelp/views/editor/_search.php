<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textAreaRow($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textFieldRow($model,'tsCreated',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'userIdCreated',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'tsUpdated',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'userIdUpdated',array('class'=>'span5')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>Yii::t('onlinehelp','Search'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>
