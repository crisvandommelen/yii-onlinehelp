<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'online-help-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions'=>array('class'=>'well'),
    'focus'=>array($model,$model->isNewRecord ? 'title' : 'content'),
)); ?>

    <p class="help-block"><?php echo sprintf(Chtml::encode(Yii::t('onlinehelp','Fields with %s are required.')),'<span class="required">*</span>'); ?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255, 'title'=>Yii::t('onlinehelp','Enter a title for this online help'))); ?>

    <?php echo $form->textFieldRow($model,'requestId',array('class'=>'span5','maxlength'=>255, 'title'=>Yii::t('onlinehelp','Enter a Yii request ID, e.g. post/admin here'))); ?>
    
    <?php echo $form->checkBoxRow($model,'isPageHelp', array('title'=>Yii::t('onlinehelp','Check if this help is for a page generally'))); ?>

    <?php echo $form->textFieldRow($model,'sequenceNbr',array('class'=>'span2', 'title'=>Yii::t('onlinehelp','This number is used to display consecutive help guiders in a defined sequence'))); ?>

    <?php echo $form->textFieldRow($model,'elementId',array('class'=>'span3', 'title'=>Yii::t('onlinehelp','If this is not a page help, enter a jQuery selector for the element the help guider shall be attached to.') )); ?>

    <?php echo $form->dropDownListRow($model,'guiderPosition', Onlinehelp::model()->guiderPositionOptions, array('class'=>'span3', 'empty'=>'(central)')); ?>

    <div class="control-group">
        <?php echo CHtml::activeLabelEx($model,'content',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php $this->widget('application.extensions.diggindata.ddeditor.DDEditor',array(
                'model'=>$model,
                'attribute'=>'content',
                'previewRequest'=>'/onlinehelp/editor/preview',
                'htmlOptions'=>array('rows'=>7,'cssClass'=>'span6','style'=>'font-family:Courier', 'title'=>Yii::t('onlinehelp','Enter the help text (you may use Mardown syntax)') ),
                'additionalSnippets'=>array(
                    Yii::t('onlinehelp','Icons')=>array(
                        '<i class="icon-star"></i>' => Yii::t('onlinehelp','Create New Item'),
                        '<i class="icon-eye-open"></i>' => Yii::t('onlinehelp','View Item'),
                        '<i class="icon-pencil"></i>' => Yii::t('onlinehelp','Update Item'),
                        '<i class="icon-remove"></i>' => Yii::t('onlinehelp','Remove Item'),
                        '<i class="icon-th-list"></i>' => Yii::t('onlinehelp','Manage Items'),
                        '<i class="icon-th-large"></i>' => Yii::t('onlinehelp','List Items'),
                    ),
                    //Yii::t('onlinehelp','App. Pages') => $pages,
                ),
            )); ?>
            <?php echo CHtml::error($model,'content'); ?>    
        </div>
    </div>
    
    <?php if(!$model->isNewRecord) echo $form->checkBoxRow( $model, 'saveAsNew', array( 'title'=>Yii::t('onlinehelp','Save as a new item, keeping the original one')) ); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('onlinehelp','Create') : Yii::t('onlinehelp','Save'),
            'icon'=>'ok white',
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'reset', 
            'icon'=>'repeat', 
            'label'=>Yii::t('onlinehelp','Reset'))); ?>
    </div>

<?php $this->endWidget(); ?>


</div><!-- form -->
