<?php

class EditorController extends Controller
{
    // {{{ *** Members ***
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    public $defaultAction = 'admin';
    // }}} End Members
    // {{{ filters
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    } // }}} 
    // {{{ accessRules
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array();
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','admin','delete','preview'),
                'users'=>array('@'),
            ),
            /*
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            */
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    } // }}} 
    // {{{ *** Actions ***
    // {{{ actionView
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    } // }}} 
    // {{{ actionCreate
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        // $this->checkAccess('onlinehelpitem.create');
        $model=new OnlineHelp;
        if(isset($_GET['OnlineHelp'])) {
            $model->attributes = $_GET['OnlineHelp'];
            $model->requestId = str_replace('.', '/', $model->requestId);
            $model->isPageHelp = true;
        }

        $model->requestId = preg_replace("/\/id\/.*$/",'',$model->requestId);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OnlineHelp']))
        {
            $model->attributes=$_POST['OnlineHelp'];
            if($model->save()) {
                $msg = array(
                    '<h4>',
                    Yii::t('onlinehelp','Creating New Online Help Page'),
                    '</h4>',
                    Yii::t('onlinehelp','The Online Help Page {recordName} has been created.',array('{recordName}'=>$model->recordName))
                );
                if(preg_match("/update|view/",$model->requestId))
                    $msg[] = Yii::t('onlinehelp','The request ID contains view or update. To see this help text, open the respective create or update page.');
                Yii::app()->user->setFlash( 'success', join( "\n", $msg ) );
                $requestId = preg_replace("/(update|view.*)$/",'admin',$model->requestId);
                $this->redirect(array('/'.$requestId));
            } else {
                Yii::app()->user->setFlash(
                    'error',
                    array(
                        Yii::t('onlinehelp','Error Creating New Online Help Page'),
                        Yii::t('onlinehelp','Couldn\' create new Online Help Page.')
                    )
                );
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    } // }}} 
    // {{{ actionUpdate
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        // $this->checkAccess('onlinehelpitem.update');
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['OnlineHelp']))
        {
            $model->attributes=$_POST['OnlineHelp'];
            if($model->saveAsNew==true) {
                $model = new OnlineHelp;
                $model->attributes=$_POST['OnlineHelp'];
                unset($model->id);
            }
            if($model->save()) {
                $msg = array(
                    '<h4>',
                    Yii::t('onlinehelp','Updating an Online Help Page'),
                    '</h4>',
                    Yii::t('onlinehelp','The Online Help Page {recordName} has been updated.',array('{recordName}'=>$model->recordName))
                );
                if(preg_match("/update|view/",$model->requestId))
                    $msg[] = Yii::t('onlinehelp','The request ID contains view or update. To see this help text, open the respective create or update page.');
                Yii::app()->user->setFlash( 'success', join( "\n", $msg ) );
                $requestId = preg_replace("/(update|view.*)$/",'admin',$model->requestId);
                $this->redirect(array('/'.$requestId));
            } else {
                Yii::app()->user->setFlash(
                    'error',
                    array(
                        Yii::t('onlinehelp','Error Updating Online Help Page'),
                        Yii::t('onlinehelp','Couldn\' update Online Help Page {recordName}.',array('{recordName}'=>$model->recordName))
                    )
                );
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    } // }}}
    // {{{ actionPreview
    public function actionPreview()
    {
        $parser=new CMarkdownParser;
        echo $parser->safeTransform($_POST['OnlineHelp'][$_GET['attribute']]);
    } // }}} 
    // {{{ actionDelete
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // $this->checkAccess('onlinehelpitem.delete');
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $result = $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax'])) {
                if($result==true) {
                    Yii::app()->user->setFlash(
                        'success',
                        array(
                            Yii::t('onlinehelp','Deleting Online Help Page'),
                            Yii::t('onlinehelp','The Online Help Page {recordName} has been deleted.',array('{recordName}'=>$model->recordName))
                        )
                    );
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                } else {
                    Yii::app()->user->setFlash(
                        'error',
                        array(
                            Yii::t('onlinehelp','Error Deleting Online Help Page'),
                            Yii::t('onlinehelp','The Online Help Page {recordName} couldn\'t be deleted.',array('{recordName}'=>$model->recordName))
                        )
                    );
                }
            }
        }
        else
            throw new CHttpException(400,Yii::t('onlinehelp','Invalid request. Please do not repeat this request again.'));
    } // }}} 
    // {{{ actionIndex
    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        // $this->checkAccess('onlinehelpitem.list');
        $dataProvider=new CActiveDataProvider('OnlineHelp');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    } // }}} 
    // {{{ actionAdmin
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        //$this->checkAccess('onlinehelpitem.admin');
        $model=new OnlineHelp('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['OnlineHelp']))
            $model->attributes=$_GET['OnlineHelp'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    } // }}}
    // {{{ actionToggleUserOnlineHelpPage
    public function actionToggleUserOnlineHelpPage($requestId, $isChecked)
    {
        $requestId = str_replace('.', '/', $requestId);
        if( $isChecked=='checked' )
            $isChecked = 1;
        else
            $isChecked = 0;
        if($isChecked==0) {
            $sql = "INSERT INTO `{{useronlinehelptohide}}` (userId, requestId) VALUES ( :userId, :requestId )";
        } else {
            $sql = "DELETE FROM `{{useronlinehelptohide}}` WHERE userId=:userId AND requestId=:requestId";
        }
        $userIdColumn = Yii::app()->modules['onlinehelp']['userIdColumn'];
        if(is_null($userIdColumn)) {
            $module = get_class_vars ( 'OnlinehelpModule' );
            $userIdColumn = $module['userIdColumn'];
        }
        $result = Yii::app()->db->createCommand($sql)->execute( array(':userId'=>Yii::app()->user->{$userIdColumn}, ':requestId'=>$requestId) );
        echo CJSON::encode(array('data'=>'requestId: '.$requestId.', isChecked: '.$isChecked, 'status'=>'SUCCESS'));
    } // }}} 
    // }}} End Actions 
    // {{{ *** Other Methods ***
    // {{{ loadModel
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=OnlineHelp::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,Yii::t('onlinehelp','The requested page does not exist.'));
        return $model;
    } // }}} 
    // {{{ performAjaxValidation
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='online-help-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    } // }}} 
    // }}} End Other Methods
}
