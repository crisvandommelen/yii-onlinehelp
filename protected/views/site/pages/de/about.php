<?php
$this->breadcrumbs=array(
	'Über',
);
?>

<h1>Über <em><?php echo CHtml::encode(Yii::app()->name); ?></em></h1>

<p>Dies ist die "Über..."-Seite für ein Blog-System mit dem <a href="www.yiiframework.com">Yii Framework</a>.</p>

<p>Das Blog-System dient zur Demonstration der <a href="http://www.yiiframework.com/extensions/yii-onlinehelp">yii-onlinehelp-Extension</a>.</p>
