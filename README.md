Yii Bootstrap Blog Demo with Onlinehelp
=======================================

This repository contains a version of the [Yii Blog Demo][YiiBlog].

It adds the [Yii bootstrap][yii-bootstrap] and the [Yii onlinehelp][yii-onlinehelp] extensions.

To view a demo, see [here][dd-onlinehelp-demo].

Read more about the **onlinehelp** extension in the sourcecode (***protected/modules/onlinehelp/README.mk***).

[dd-onlinehelp-demo]: http://www.diggin-data.de/demos/onlinehelp
[yii-blog]: http://www.yiiframework.com/doc/blog/ "Building a Blog System using Yii"
[yii-bootstrap]: http://www.yiiframework.com/extension/bootstrap/
[yii-onlinehelp]: http://www.yiiframework.com/extension/onlinehelp/
